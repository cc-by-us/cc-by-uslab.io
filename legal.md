---
layout: page
title: Privacy / Imprint
---
<div class="col-lg-12 text-center">
	<h2 class="section-heading text-uppercase">Privacy / Imprint</h2>
</div>

This Privacy Policy describes how your personal information is collected, used, and shared when you visit {{ site.title }} (the “Site”).

**PERSONAL INFORMATION WE COLLECT**

We do not collect any data about you or use any cookies.

**CHANGES**

We may update this privacy policy from time to time for personal, operational, legal, or regulatory reasons.

**CONTACT US**

For more information about our privacy practices or if you have questions, please contact us by email at <a href="mailto:{{ site.email }}">{{ site.email }}</a>.

_Responsible for the content:_

Technische Universtität Dresden

Professur für Geoinformatik

Dr. Daniel Nüst

Helmholtzstr. 10

D-01069 Dresden

Email: [daniel.nuest@tu-dresden.de](mailto:daniel.nuest@tu-dresden.de)
