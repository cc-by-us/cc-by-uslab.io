---
title: Use Cases Survey
subtitle: "Please fill out the survey and add your use case at **<https://t1p.de/cc-cases>**"
image: assets/img/portfolio/survey.jpg
# https://pixabay.com/de/photos/umfrage-meinungsforschung-befragung-1594962/
alt: hand filling out a form with a pen

caption:
  title: Use Cases Survey
  subtitle: "Contribute your use case of cultural change: **<https://t1p.de/cc-cases>**"
  thumbnail: assets/img/portfolio/survey.jpg
---

_Sammlung von USE CASES_

Für die Reihe CC-BY-US: Forschungsdaten teilen mit und durch die NFDI suchen wir nach USE CASES und Fallbeispielen. Wir möchten Eure Beispiele für (gelungenen) Kulturwandel in Richtung Open Science und beim Teilen von Forschungsdaten sammeln und teilen: Unser Ziel ist es, die Dimensionen des ablaufenden Kulturwandels besser zu verstehen und anderen zu helfen, daraus zu lernen.
Deshalb freuen wir uns über USE CASES aus allen Fachrichtungen und Konsortien.
Zur ersten Orientierung möchten wir Ihnen drei Leitfragen und vier konkrete Erkenntnisziele mitgeben:

**Was kann ein Use Case sein: Leitfragen**

Fallbeispiele als Erfolgsgeschichte eines gelungenen Cultural Change?

Rolle, die Infrastrukturen als Ermöglicher, aber auch Treiber von Kulturwandel dabei spiel(t)en?

Entwicklungen, die Nutzung von Infrastrukturen gefördert bzw. erschwert oder gar verhindert haben?

**Ziele, die diese Sammlung von Use Cases untertützen soll**

Dimensionen von Cultural Change verstehen

Learnings für Herausforderungen austauschen

Unterstützungsdimensionen für die NFDI im Bereich Cultural Change erschließen

Rückwirkungen auf Infrastrukturen einordnen

**Fill out the survey at <https://t1p.de/cc-cases>**
