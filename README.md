
# CC-BY-US

![Build Status](https://gitlab.com/cc-by-us/cc-by-us.gitlab.io/badges/master/build.svg)
![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

**<https://culturalchange.science>**

---

## Editing texts

Got to the file `_data/sitetext.yml` and edit the existing texts and contents, e.g., add a further timeline item or add more people to the team.

To _add a publication_ duplicate and adjust the existing files in the directory `_portfolio/`.

Look at existing images for the required sizes (timeline, portfolio).

## GitLab CI

This project's static Pages are built by GitLab CI, following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```yaml
image: ruby:2.3

variables:
  JEKYLL_ENV: production

pages:
  script:
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master
```

## Using Jekyll locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Install [Jekyll](http://jekyllrb.com/)
1. Download dependencies: `bundle`
1. Build and preview: `bundle exec jekyll serve`
1. Add content

Read more at Jekyll's [documentation](https://jekyllrb.com/docs/).

## Development

Theme: <https://github.com/raviriley/agency-jekyll-theme> using <https://github.com/raviriley/agency-jekyll-theme-starter>.

Updated Fontawesome to 5.14.5-web Free to have the ORCID icon.

## License

Except where otherwise noted site content created by the CC-BY-US initiative on this website is licensed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/) (CC-BY 4.0).
